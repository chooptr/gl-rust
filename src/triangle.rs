#![allow(dead_code)]

use std::ffi::CString;

use crate::render_gl::{self, buffer};
use crate::render_gl::data::{f32_f32_f32, u2_u10_u10_u10_rev_float};
use crate::resources::Resources;

#[derive(VertexAttribPointers, Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct Vertex {
	#[location = "0"]
	pub pos: f32_f32_f32,
	#[location = "1"]
	pub clr: u2_u10_u10_u10_rev_float,
}

pub struct Triangle {
	program: render_gl::program::Program,
	_vbo: render_gl::buffer::ArrayBuffer,
	vao: render_gl::buffer::VertexArray,
	gl: gl::Gl,
}

impl Triangle {
	pub fn new(res: &Resources, gl: &gl::Gl) -> Result<Triangle, failure::Error> {
		// Attach and link program
		let program = render_gl::Program::from_res(gl, res, "shaders/triangle")?;

		// Setup vertices
		let vertices: Vec<Vertex> = vec![
			Vertex {
				pos: (0.5, -0.5, 0.0).into(),
				clr: (1.0, 0.0, 0.0, 1.0).into(),
			}, // bottom right
			Vertex {
				pos: (-0.5, -0.5, 0.0).into(),
				clr: (0.0, 1.0, 0.0, 1.0).into(),
			}, // bottom left
			Vertex {
				pos: (0.0, 0.5, 0.0).into(),
				clr: (0.0, 0.0, 1.0, 1.0).into(),
			}  // top
		];

		// VAO
		let vao = buffer::VertexArray::new(gl);
		vao.bind();

		// VBO
		let vbo = buffer::ArrayBuffer::new(gl);
		vbo.bind();
		vbo.static_draw_data(&vertices);
		Vertex::vertex_attrib_pointers(gl);
		vbo.unbind();
		vao.unbind();

		Ok(Triangle {
			program,
			_vbo: vbo,
			vao,
			gl: gl.clone(),
		})
	}

	pub fn render(&self, mvp: glm::Mat4) {
		self.program.set_active();
		self.vao.bind();

		unsafe {
			let u_mvp = self.gl.GetUniformLocation(self.program.id,
			                                       CString::new("mvp").expect("bka?").as_bytes_with_nul().as_ptr() as *const i8);
			self.gl.UniformMatrix4fv(u_mvp, 1, gl::FALSE, mvp.as_slice().as_ptr() as *const f32);

			self.gl.DrawArrays(
				gl::TRIANGLES,
				0,
				3,
			);
		}
	}
}