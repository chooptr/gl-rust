use std::ffi::{CStr, CString};

use crate::error::Error;
use crate::resources::{Resources};
use failure::Fail;


pub struct Shader {
	gl: gl::Gl,
	id: gl::types::GLuint,
}

impl Shader {
	pub fn id(&self) -> gl::types::GLuint {
		self.id
	}

	pub fn from_source(gl: &gl::Gl, source: &CStr, shader_type: gl::types::GLenum) -> Result<Shader, String> {
		let id = shader_from_source(gl, source, shader_type)?;
		Ok(Shader { gl: gl.clone(), id })
	}

	pub fn from_res(gl: &gl::Gl, res: &Resources, name: &str) -> Result<Shader, Error> {
		const POSSIBLE_EXT: [(&str, gl::types::GLenum); 2] = [
			(".vert", gl::VERTEX_SHADER),
			(".frag", gl::FRAGMENT_SHADER),
		];

		let shader_kind = POSSIBLE_EXT.iter()
		                              .find(|&&(file_extension, _)| {
			                              name.ends_with(file_extension)
		                              })
		                              .map(|&(_, kind)| kind)
		                              .ok_or_else(|| Error::CanNotDetermineShaderTypeForResource { name: name.into() })?;

		let source = res.load_cstring(name)
		                .map_err(|e| Error::ResourceLoad { name: format!("{} {:?}", name, e.name()) })?;

		Shader::from_source(gl, &source, shader_kind).map_err(|message| Error::CompileError { name: name.into(), message })
	}
}

impl Drop for Shader {
	fn drop(&mut self) {
		unsafe {
			self.gl.DeleteShader(self.id);
		}
	}
}

pub fn create_whitespace_cstring_with_len(len: usize) -> CString {
	let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
	buffer.extend([b' '].iter().cycle().take(len));
	unsafe { CString::from_vec_unchecked(buffer) }
}

fn shader_from_source(gl: &gl::Gl, source: &CStr, shader_type: gl::types::GLuint) -> Result<gl::types::GLuint, String> {
	let id = unsafe { gl.CreateShader(shader_type) };
	unsafe {
		gl.ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
		gl.CompileShader(id);
	}

	{ // Error handling
		let mut success: gl::types::GLint = 1;
		unsafe { gl.GetShaderiv(id, gl::COMPILE_STATUS, &mut success); }
		if success == 0 {
			let mut len: gl::types::GLint = 0;
			unsafe { gl.GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len); }

			let error = create_whitespace_cstring_with_len(len as usize);
			unsafe {
				gl.GetShaderInfoLog(
					id,
					len,
					std::ptr::null_mut(),
					error.as_ptr() as *mut gl::types::GLchar,
				);
			}
			return Err(error.to_string_lossy().into_owned());
		}
	}

	Ok(id)
}
