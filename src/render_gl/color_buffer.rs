use gl;

use crate::render_gl::data;

pub struct ColorBuffer {
	pub color: data::u2_u10_u10_u10_rev_float,
	gl: gl::Gl,
}

impl ColorBuffer {
	pub fn from_color(color: data::u2_u10_u10_u10_rev_float, gl: &gl::Gl) -> ColorBuffer {
		ColorBuffer {
			color: (color.inner.x(), color.inner.y(), color.inner.z(), color.inner.w()).into(),
			gl: gl.clone(),
		}
	}

	pub fn _update_color(&mut self, color: data::u2_u10_u10_u10_rev_float) {
		self.color = (color.inner.x(), color.inner.y(), color.inner.z(), color.inner.w()).into();
	}

	pub fn set_used(&self) {
		unsafe { self.gl.ClearColor(self.color.inner.x(), self.color.inner.y(), self.color.inner.z(), self.color.inner.w()); }
	}

	pub fn clear(&self) {
		unsafe { self.gl.Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT); }
	}
}