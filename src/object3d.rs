use std::ffi::CString;

use nalgebra_glm as glm;

use crate::render_gl::{self, buffer, data};
use crate::resources::Resources;


#[derive(VertexAttribPointers, Copy, Clone, Debug)]
#[repr(C, packed)]
struct Vertex {
	#[location = "0"]
	pub pos: data::f32_f32_f32,
	#[location = "1"]
	pub n: data::f32_f32_f32,
	#[location = "2"]
	pub uv: data::f16_f16,
}

pub struct Object3D {
	program: render_gl::Program,
	vao: render_gl::buffer::VertexArray,
	texture: Option<render_gl::Texture>,
	count: usize,
	gl: gl::Gl,
}

impl Object3D {
	pub fn new(res: &Resources, gl: &gl::Gl) -> Result<Object3D, failure::Error> {
		// Attach and link program
		let program = render_gl::Program::from_res(gl, res, "shaders/cube")?;

		let (models, materials) = res.load_obj("obj/dice.obj")?;
//		let (models, materials) = res.load_obj("obj/bb8/bb8.obj")?;

		let material = materials.iter().next();
		let texture = match material {
			Some(material) => if &material.diffuse_texture == "" {
				None
			} else {
				Some(
					render_gl::Texture::from_res_rgb(
						&["textures/", &material.diffuse_texture[..], ].concat(),
					).load(gl, res)?,
				)
			},
			None => None,
		};

		let vbo_data = models.iter()
		                     .map(|m| &m.mesh)
		                     .flat_map(|m| {
			                     m.positions.chunks(3)
			                      .zip(m.normals.chunks(3))
			                      .zip(m.texcoords.chunks(2))
			                      .map(|((p, n), t)| Vertex {
				                      pos: (p[0], p[1], p[2]).into(),
				                      n: (n[0], n[1], n[2]).into(),
				                      uv: (t[0], -t[1]).into(),
			                      })
			                      .collect::<Vec<_>>()
		                     })
		                     .collect::<Vec<_>>();

		let ebo_data = &models.iter().enumerate()
		                      .map(|(i, m)| (i, m.mesh.indices.clone()))
		                      .flat_map(|(i, indices)| {
			                      if i > 0 {
				                      return indices.iter().map(|&idx| {
					                      idx + models[0..i - 1].iter().fold(0, |x, y| {
						                      x + y.mesh.positions.len() as u32
					                      })
				                      }).collect::<Vec<_>>();
			                      }
			                      return indices;
		                      })
		                      .collect::<Vec<_>>();

		// VAO
		let vao = buffer::VertexArray::new(&gl);
		vao.bind();

		// VBO
		let vbo = buffer::ArrayBuffer::new(&gl);
		vbo.bind();
		vbo.static_draw_data(&vbo_data);

		// EBO
		let ebo = buffer::ElementArrayBuffer::new(&gl);
		ebo.bind();
		ebo.static_draw_data(&ebo_data);

		vao.bind();
		vbo.bind();
		ebo.bind();

		Vertex::vertex_attrib_pointers(&gl);
		vao.unbind();
		vbo.unbind();
		ebo.unbind();

		Ok(Object3D {
			program,
			vao,
			texture,
			count: ebo_data.len(),
			gl: gl.clone()
		})
	}

	pub fn render(&self, p: &glm::Mat4, v: &glm::Mat4, m: &glm::Mat4, cam_pos: &glm::Vec3) {
		self.program.set_active();
		self.vao.bind();

		unsafe {
			let u_p = self.gl.GetUniformLocation(self.program.id,
			                                     CString::new("P").expect("Uniform not found: P")
			                                                      .as_bytes_with_nul()
			                                                      .as_ptr() as *const i8);
			self.gl.UniformMatrix4fv(u_p, 1, gl::FALSE, p.as_slice().as_ptr() as *const f32);

			let u_v = self.gl.GetUniformLocation(self.program.id,
			                                     CString::new("V").expect("Uniform not found: V")
			                                                      .as_bytes_with_nul()
			                                                      .as_ptr() as *const i8);
			self.gl.UniformMatrix4fv(u_v, 1, gl::FALSE, v.as_slice().as_ptr() as *const f32);

			let u_m = self.gl.GetUniformLocation(self.program.id,
			                                     CString::new("M").expect("Uniform not found: M")
			                                                      .as_bytes_with_nul()
			                                                      .as_ptr() as *const i8);
			self.gl.UniformMatrix4fv(u_m, 1, gl::FALSE, m.as_slice().as_ptr() as *const f32);

			let u_cam = self.gl.GetUniformLocation(self.program.id,
			                                       CString::new("cam_pos").expect("Uniform not found: campos")
				                                       .as_bytes_with_nul()
				                                       .as_ptr() as *const i8);
			self.gl.Uniform3f(u_cam, cam_pos.x, cam_pos.y, cam_pos.z);

			let u_tex = self.gl.GetUniformLocation(self.program.id,
			                                       CString::new("tex_face").expect("Uniform not found: tex_face")
			                                                              .as_bytes_with_nul()
			                                                              .as_ptr() as *const i8);
			if let &Some(ref texture) = &self.texture {
				texture.bind_at(0);
				self.gl.Uniform1i(u_tex, 0);
			}


			self.gl.DrawElements(
				gl::TRIANGLES,
				self.count as i32,
				gl::UNSIGNED_INT,
				std::ptr::null(),
			);
//			self.gl.DrawArrays(
//				gl::TRIANGLES,
//				0,
//				self.count as i32,
//			);
		}
	}
}