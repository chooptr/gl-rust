use image;
use std::ffi::CString;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

use crate::error::Error;


pub struct Resources {
	root_path: PathBuf
}

impl Resources {
	pub fn from_relative_exe_path(rel_path: &Path) -> Result<Resources, Error> {
		let exe_file_name = ::std::env::current_exe().map_err(|_| Error::FailedToGetExePath)?;
		let exe_path = exe_file_name.parent().ok_or(Error::FailedToGetExePath)?;

		Ok(Resources { root_path: exe_path.join(rel_path) })
	}

	pub fn load_cstring(&self, resource_name: &str) -> Result<CString, Error> {
		let mut file = File::open(Resources::resource_name_to_path(&self.root_path, resource_name))?;
		let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize + 1);
		file.read_to_end(&mut buffer)?;

		if buffer.iter().find(|i| **i == 0).is_some() {
			return Err(Error::FileContainsNil);
		}

		Ok(unsafe { CString::from_vec_unchecked(buffer) })
	}

	fn resource_name_to_path(root_dir: &Path, location: &str) -> PathBuf {
		let mut path: PathBuf = root_dir.into();

		for part in location.split("/") {
			path = path.join(part);
		}

		path
	}

	pub fn load_rgb_image(&self, image_file_name: &str) -> Result<image::RgbImage, Error> {
		let img = image::open(Resources::resource_name_to_path(&self.root_path, image_file_name))?;

		Ok(img.to_rgb())
	}

	pub fn load_rgba_image(&self, image_file_name: &str) -> Result<image::RgbaImage, Error> {
		let img = image::open(Resources::resource_name_to_path(&self.root_path, image_file_name))?;

		if let image::ColorType::RGBA(_) = img.color() {
			Ok(img.to_rgba())
		} else {
			Err(Error::ImageIsNotRgba {
				name: image_file_name.into(),
			})
		}
	}

	pub fn load_obj(&self, obj_file_name: &str) -> tobj::LoadResult {
		let r = Resources::resource_name_to_path(&self.root_path, obj_file_name);
		tobj::load_obj(r.as_path())

//		let f = File::open(format!("assets/{}", obj_file_name))?;
//		let mut buf = BufReader::new(f);
//		obj::load_obj(buf)
	}
}