#[macro_use]
extern crate failure;
extern crate nalgebra_glm as glm;
#[macro_use]
extern crate render_gl_derive;
extern crate sdl2;
//extern crate obj;
extern crate tobj;
extern crate vec_2_10_10_10;
extern crate half;
extern crate image;

use std::path::Path;

use render_gl::color_buffer::ColorBuffer;
use resources::Resources;

use crate::object3d::Object3D;
use crate::debug::failure_to_string;

mod object3d;
mod debug;
mod error;
mod triangle;
mod render_gl;
mod resources;

fn main() {
	if let Err(e) = run() {
		println!("{}", failure_to_string(e));
	}
}

fn run() -> Result<(), failure::Error> {
	let res = Resources::from_relative_exe_path(Path::new("assets")).unwrap();

	let sdl = sdl2::init().unwrap();
	let video_subsystem = sdl.video().unwrap();
	let gl_attr = video_subsystem.gl_attr();
	gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
	gl_attr.set_context_version(4, 5);

	let window = video_subsystem
		.window("Game", 900, 700)
		.opengl()
		.resizable()
		.build()
		.unwrap();

	let _gl_context = window.gl_create_context().unwrap();

	let gl = gl::Gl::load_with(|s| {
		video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void
	});

	let mut viewport = render_gl::Viewport::for_window(900, 700, &gl);
	let color_buffer = ColorBuffer::from_color((0.3, 0.3, 0.5).into(), &gl);
	let cube = Object3D::new(&res, &gl)?;

	viewport.set_used();
	color_buffer.set_used();

	unsafe {
		gl.Enable(gl::CULL_FACE);
		gl.Enable(gl::DEPTH_TEST);
	}

	let projection = glm::perspective(1.0, 900.0 / 700.0, 0.1, 100.0);

//	let mut viewi;// = glm::look_at(
//		&glm::vec3(0.0, 0.0, 10.0),
//		&glm::vec3(0.0, 0.0, 0.0),
//		&glm::vec3(0.0, 1.0, 0.0));

	let model = glm::scale(
		&glm::mat4(1.0, 0.0, 0.0, 0.0,
		           0.0, 1.0, 0.0, 0.0,
		           0.0, 0.0, 1.0, 0.0,
		           0.0, 0.0, 0.0, 1.0),
		&glm::vec3(0.9f32, 0.9f32, 0.9f32),
	);
	let model = glm::translate(&model, &glm::vec3(0.0, 0.0, 0.0));

	let mut step = 0f32;

	let mut event_pump = sdl.event_pump().unwrap();
	'main: loop {
		if step >= 2.0 * 3.14 {
			step = 0f32;
		}
		for event in event_pump.poll_iter() {
			match event {
				sdl2::event::Event::Quit { .. } => break 'main,

				sdl2::event::Event::Window {
					win_event: sdl2::event::WindowEvent::Resized(w, h),
					..
				} => {
					viewport.update_size(w, h);
					viewport.set_used();
				}
				_ => {}
			}
		}

		let cam_pos = glm::vec3(step.cos() * 10.0, 0.0, step.sin() * 10.0);
		let view = glm::look_at(
			&cam_pos,
			&glm::vec3(0.0, 0.0, 0.0),
			&glm::vec3(0.0, 1.0, 0.0),
		);
		color_buffer.clear();
		cube.render(&projection, &view, &model, &cam_pos);

		window.gl_swap_window();
		step += 0.005;
	}
	Ok(())
}


