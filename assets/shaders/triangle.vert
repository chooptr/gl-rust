#version 330 core

uniform mat4 mvp;

layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;

out VS_OUTPUT {
    vec4 color;
} OUT;

void main()
{
    gl_Position = mvp * vec4(position, 1.0);
    OUT.color = color;
}