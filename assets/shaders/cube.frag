#version 450

uniform sampler2D tex_face;

in VS_OUTPUT {
    vec3 camera_pos;
    vec3 position;
    vec3 normal;
    vec2 uv;
} IN;

out vec4 out_color;

void main()
{
    vec3 light_pos = vec3(0.0, 0.0, 0.0);
    vec3 color = texture(tex_face, IN.uv).rgb;
    vec3 normal = IN.normal;

    // diffuse
    vec3 light_dir = normalize(light_pos - IN.position);
    float diff = max(dot(light_dir, normal), 0.0);
    vec3 diffuse = diff * color;

    // specular
    vec3 view_dir = normalize(IN.camera_pos - IN.position);
    vec3 reflect_dir = reflect(-light_dir, normal);
    vec3 halfway_dir = normalize(light_dir + view_dir);
    float spec = pow(max(dot(normal, halfway_dir), 0.0), 8.0);
    vec3 specular = vec3(0.2) * spec;

//    out_color = vec4(mix(diffuse, specular, 0.3), 1.0);
    out_color = texture(tex_face, IN.uv);
}
