#version 450

uniform vec3 camera_pos;
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

out VS_OUTPUT {
    vec3 camera_pos;
    vec3 position;
    vec3 normal;
    vec2 uv;
} OUT;

void main()
{
    gl_Position = P * V * M * vec4(position, 1.0);

    OUT.camera_pos = camera_pos;
    OUT.position = position;
    OUT.normal = normal;
    OUT.uv = uv;
}
